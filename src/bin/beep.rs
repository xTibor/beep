use std::fs::OpenOptions;
use std::io::Write;
use std::{thread, time};

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "beep")]
struct Opt {
    /// Beep frequency in Hz
    #[structopt(short = "f", long = "frequency", default_value = "440")]
    frequency: u16,

    /// Beep length in milliseconds
    #[structopt(short = "d", long = "duration", default_value = "250")]
    duration: u16,
}

fn main() {
    let opt = Opt::from_args();

    let mut file = OpenOptions::new()
        .write(true)
        .open("pcspkr:")
        .expect("beep: failed to open pcspkr:");

    // TODO: 1.32.0
    //file.write(opt.frequency.to_ne_bytes())
    //    .expect("beep: failed to write frequency to pcspkr:");
    file.write(&[
        (opt.frequency & 0xFF) as u8,
        ((opt.frequency >> 8) & 0xFF) as u8,
    ])
    .expect("beep: failed to write beep frequency to pcspkr:");

    let duration = time::Duration::from_millis(opt.duration as u64);
    thread::sleep(duration);
}
