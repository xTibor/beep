use std::fs::{self, OpenOptions};
use std::io::Write;
use std::{thread, time};

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "system-beeps")]
struct Opt {
    /// Binary file containing the song data
    #[structopt(short = "i", long = "path")]
    path: String,

    #[structopt(short = "l", long = "loop")]
    loop_song: bool,
}

struct State {
    position: usize,
    loop_position: usize,
    wait: u8,
    framerate: u64,
    done: bool,
}

fn main() {
    let opt = Opt::from_args();

    let mut pcspkr = OpenOptions::new()
        .write(true)
        .open("pcspkr:")
        .expect("system-beeps: failed to open pcspkr:");

    let data = fs::read(&opt.path)
        .expect("system-beeps: failed to read song data");

    let mut state = State {
        position: 1,
        loop_position: 0,
        wait: 0,
        framerate: data[0] as u64,
        done: false,
    };

    let mut update = |state: &mut State| {
        let delta = data[state.position];
        state.position += 1;

        if delta == 0x00 {
            state.done = true;
        } else if delta == 0xFF {
            if state.loop_position == 0 {
                state.loop_position = state.position;
            } else {
                if opt.loop_song {
                    state.position = state.loop_position;
                }
            }
        } else {
            // TODO: Use byteorder functions
            state.wait = delta;
            let pit_period = (data[state.position + 0] as usize) + ((data[state.position + 1] as usize) << 8);
            state.position += 2;

            // Needs explicit usize, otherwise the compiler infers wrong type and gets silently truncated
            let frequency: usize = (0x1234DC as usize).checked_div(pit_period).unwrap_or(0);
            println!("{:08X}", frequency);

            // TODO: 1.32.0
            pcspkr.write(&[
                (frequency & 0xFF) as u8,
                ((frequency >> 8) & 0xFF) as u8,
            ])
            .expect("system-beeps: failed to write beep frequency to pcspkr:");
        }
    };

    while !state.done {
        let frame_start = time::Instant::now();

        if state.wait == 0 {
            update(&mut state);
        } else {
            state.wait -= 1;
            if state.wait == 0 {
                update(&mut state);
            }
        }

        // TODO: Improve timing
        let target_duration = time::Duration::from_micros(1000000 / state.framerate);
        let frame_duration = frame_start.elapsed();

        if let Some(sleep_duration) = target_duration.checked_sub(frame_duration) {
            thread::sleep(sleep_duration);
        } else {
            println!("Warning: frame={:?}, target={:?}", frame_duration, target_duration);
        }
    }
}
